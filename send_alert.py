#!/usr/bin/env python
"""
FENWAY COMMUTE ALERTS

Copyright (c) 2016 Erin Morelli

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.
"""

import json
import urllib
import smtplib
import httplib
from os import path
from datetime import datetime

import yaml
from pyrfc3339 import parse
from googlevoice import Voice
from apiclient import discovery


# Set root directory
ROOT_DIR = path.dirname(__file__)

# Config file setup
CONFIG_FILE = path.join(ROOT_DIR, 'config.yaml')
CONFIG = yaml.load(open(CONFIG_FILE).read())

# Calendar API service
SCHEDULE_SERVICE = discovery.build(
    'calendar',
    'v3',
    developerKey=CONFIG['google']['api_key']
)

# Get the current date
TODAY = datetime.today().date()


def check_schedule():
    ''' Checks Google Calendars for any event happening today
    '''
    page_token = None
    todays_events = []

    # Check the schedules
    for calendar_id in CONFIG['google']['calendars']:
        while True:
            # Query the calendar from the API
            events = SCHEDULE_SERVICE.events().list(
                calendarId=calendar_id,
                pageToken=page_token
            ).execute()

            # Iterate over events
            for event in events['items']:
                event_date = None
                event_time = None

                if 'dateTime' in event['start'].keys():
                    event_date = parse(event['start']['dateTime']).date()
                    event_time = parse(event['start']['dateTime']).time()

                elif 'date' in event['start'].keys():
                    event_date = datetime.strptime(
                        event['start']['date'], '%Y-%m-%d').date()
                    event_time = datetime.strptime(
                        event['start']['date'], '%Y-%m-%d').time()

                if event_date == TODAY:
                    event['event_date'] = event_date
                    event['event_time'] = event_time
                    todays_events.append(event)

            # Keep loading events until we're done
            page_token = events.get('nextPageToken')

            if not page_token:
                break

    return todays_events


def get_date_suffix(day):
    ''' Sets the suffix for display of date
    '''
    if day in [11, 12, 13]:
        return 'th'
    elif day % 10 == 1:
        return 'st'
    elif day % 10 == 2:
        return 'nd'
    elif day % 10 == 3:
        return 'rd'
    else:
        return 'th'


def send_sms(subject, message):
    ''' Send alert message via SMS
    '''
    sms = CONFIG['sms']
    voice = Voice()

    # Login to Google Voice
    voice.login(sms['voice_user'], sms['voice_pass'])

    # Set message text
    if subject is None:
        text = message
    else:
        text = '{subject} {message}'.format(
            subject=subject,
            message=message
        )

    # Send SMS via Google Voice
    voice.send_sms(sms['phone'], text)


def send_push(subject, message):
    ''' Send alert message via Pushover
    '''
    conf = CONFIG['push']

    # Connect to Pushover API
    pushover = httplib.HTTPSConnection('api.pushover.net')

    # Set fallback subject
    if subject is None:
        subject = 'Commute Alerts'

    # Set up API request
    request = urllib.urlencode({
        'token': conf['token'],
        'user': conf['user'],
        'title': subject,
        'message': message,
    })

    # Make API request
    pushover.request(
        'POST',
        '/1/messages.json',
        request,
        {'Content-type': 'application/x-www-form-urlencoded'}
    )


def get_no_events_msg():
    ''' Formats message for days without events
    '''
    event_date = TODAY.strftime('%A %B {day}{suffix}')

    # Set up the message body
    message = 'There are no commute alerts for today, {today}'
    message = message.format(
        today=event_date.format(
            day=TODAY.day,
            suffix=get_date_suffix(TODAY.day)
        )
    )

    return None, message


def get_events_msg(event):
    ''' Formats message for a given event
    '''
    event_date = event['event_date'].strftime('%A %B {day}{suffix}')

    # Set up message subject
    subject = 'COMMUTE ALERT!'

    # Set up location
    location = event['location'].split(', ')[0]

    # Set up message body
    message = 'There is an event at {where} today:'.format(
        where=location,
    )
    message += ' {what} on {today} @ {when}.'.format(
        what=event['summary'],
        today=event_date.format(
            day=event['event_date'].day,
            suffix=get_date_suffix(event['event_date'].day)
        ),
        when=event['event_time'].strftime('%I:%M %p')
    )

    return subject, message


def send_alert(events):
    ''' Sends an alert via push notification
    '''
    subject = None
    message = None

    # Check for events
    if not len(events):
        (subject, message) = get_no_events_msg()

    # Otherwise send the first event
    else:
        (subject, message) = get_events_msg(events[0])

    # Send SMS if enabled in config
    if 'sms' in CONFIG.keys():
        send_sms(subject, message)

    # Send push if enabled in config
    if 'push' in CONFIG.keys():
        send_push(subject, message)


def main():
    ''' Wrapper function to run alert
    '''
    events = check_schedule()

    # Send alerts
    send_alert(events)


# Run this thing
if __name__ == '__main__':
    main()
