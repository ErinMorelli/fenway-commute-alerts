# Fenway Commute Alerts #
Made with love, by [Erin Morelli](https://www.erinmorelli.com) in Boston, Massachusetts

SMS and Pushover alerts for Fenway-area events that could affect your commute.

## Setup Instructions ##
1. Download this git repo and open the folder

    ```
    git clone https://bitbucket.org/ErinMorelli/fenway-commute-alerts.git
    ```

    ```
    cd fenway-commute-alerts
    ```

2. Install the app requirements

    ```
    pip install -r requirements.txt
    ```

3. Open the `sample_config.yaml` file with your favorite text editor and configure to suit your needs.

4. Save the configuration file as `config.yaml`

5. Schedule when you want to receive your alerts by editing your crontab

    ```
    crontab -e
    ```

6. Add a new line for the `send_alert.py` script, e.g.:

    ```
    0 16 * * 1-5 /path/to/fenway-commute-alerts/send_alert.py >/dev/null 2>&1
    ```

    This will send an alert every Weekday (Mon-Fri) at 4:00 PM.